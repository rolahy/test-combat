Hairun technology / Test vue js et Symfony - Emarolahy
=====================================

Ce dépot contient les deux projets `client` et `serveur` qui seront respéctivement testable sur le port `http://localhost:8080` et `http://localhost:8000`.

Le projet tourne sur la version 5.1.9 de Symfony et la version 3.2.13 de Vue Js.

## Bdd

* MYSQL

# Comment installer le projet?

### Cloner le projet.

```
git clone
```

### Installation des composants dans le dossier serveur
````
composer install
````

### Installation des composants dans le dossier client
````
npm install
````

