import { createRouter,createWebHistory } from "vue-router";
import signupComponent from '@/components/signup/signupComponent.vue';
import homeComponet from '@/components/home/homeComponet.vue';
import loginComponent from '@/components/login/loginComponent.vue';
import sidebarComponent from '@/components/sidebar/sidebarComponent.vue';

const routes = [
    {
        name:'signupComponent',
        path:'/signup',
        component:signupComponent,
    },
    {
        name:'homeComponet',
        path:'/',
        component:homeComponet
    },
    {
        name:'loginComponent',
        path:'/login',
        component:loginComponent
    },
    {
        name:'sidebarComponent',
        path:'/dasboard',
        component:sidebarComponent
    }
]

const router = createRouter({
history: createWebHistory(),
routes,
 })

export default router;
