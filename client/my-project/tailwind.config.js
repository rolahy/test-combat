module.exports = {
  content: [
"./src/**/*.{js,jsx,vue,tsx}",
"./node_modules/flowbite/**/*.js"
],
  theme: {
    extend: {},
  },
  plugins: [
  require('flowbite/plugin')
],
}
