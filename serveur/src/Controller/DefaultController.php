<?php

namespace App\Controller;

use App\Entity\Combat;
use App\Repository\UtilisateurRepository;
use App\Repository\CombattantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/api/login", name="login")
     */
    public function login(UtilisateurRepository $utilisateurRepository): Response
    {
        $email = $_GET['email'];
        $password = $_GET['password'];
        
        return $this->json($utilisateurRepository->findUser($email,$password));
    }

     /**
     * @Route("/api/get-fighter", name="get_fighter")
     */
    public function getFighter(CombattantRepository $combattantRepository,UtilisateurRepository $utilisateurRepository): Response
    {
        $user = $utilisateurRepository->find($_GET['id_user']);

        return $this->json($combattantRepository->findFighter($user));
    }

    /**
     * @Route("/api/fight/{id}", name="fighter")
     */
    public function fight(Combat $combat,CombattantRepository $combattantRepository): Response
    {
        $user = $_GET['id_combattant'];
        $combattant_1 = $combattantRepository->find($user);

        if($combattant_1 === $combat->getCombattant1()){
            $combat->getCombattant1()->frapper($combat->getCombattant2());
        }else{
            $combat->getCombattant2()->frapper($combat->getCombattant1());
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($combat);
        $entityManager->flush();

        return $this->json("ok");
    }

    /**
     * @Route("/api/magic/{id}", name="magic")
     */
    public function magic(Combat $combat,CombattantRepository $combattantRepository): Response
    {
        $user = $_GET['id_combattant'];
        $combattant_1 = $combattantRepository->find($user);

        if($combattant_1 === $combat->getCombattant1()){
            $combat->getCombattant1()->lancerSort($combat->getCombattant2());
        }else{
            $combat->getCombattant2()->lancerSort($combat->getCombattant1());
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($combat);
        $entityManager->flush();

        return $this->json("ok");
    }

    /**
     * @Route("/api/winner/{id}", name="winner")
     */
    public function getWinner(Combat $combat)
    {
        $winner = '';

        if($combat->getCombattant1()->getHp() < 0){
            $winner = $combat->getCombattant2()->getNom();
        }
        if($combat->getCombattant2()->getHp() < 0){
            $winner = $combat->getCombattant1()->getNom();
        }

        return $this->json($winner);
    }
}
