<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CombatRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(formats={"json"})
 * @ORM\Entity(repositoryClass=CombatRepository::class)
 */
class Combat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Combattant::class, inversedBy="combats")
     * @ORM\JoinColumn(nullable=true)
     */
    private $combattant_1;

    /**
     * @ORM\ManyToOne(targetEntity=Combattant::class, inversedBy="combats")
     * @ORM\JoinColumn(nullable=true)
     */
    private $combattant_2;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCombattant1(): ?Combattant
    {
        return $this->combattant_1;
    }

    public function setCombattant1(?Combattant $combattant_1): self
    {
        $this->combattant_1 = $combattant_1;

        return $this;
    }

    public function getCombattant2(): ?Combattant
    {
        return $this->combattant_2;
    }

    public function setCombattant2(?Combattant $combattant_2): self
    {
        $this->combattant_2 = $combattant_2;

        return $this;
    }
}
