<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UtilisateurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(formats={"json"})
 * @ORM\Entity(repositoryClass=UtilisateurRepository::class)
 */
class Utilisateur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity=Combattant::class, mappedBy="utilisateur")
     */
    private $combattant;

    public function __construct()
    {
        $this->combattant = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection|Combattant[]
     */
    public function getCombattant(): Collection
    {
        return $this->combattant;
    }

    public function addCombattant(Combattant $combattant): self
    {
        if (!$this->combattant->contains($combattant)) {
            $this->combattant[] = $combattant;
            $combattant->setUtilisateur($this);
        }

        return $this;
    }

    public function removeCombattant(Combattant $combattant): self
    {
        if ($this->combattant->removeElement($combattant)) {
            // set the owning side to null (unless already changed)
            if ($combattant->getUtilisateur() === $this) {
                $combattant->setUtilisateur(null);
            }
        }

        return $this;
    }
}
