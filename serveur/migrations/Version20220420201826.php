<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220420201826 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE combat (id INT AUTO_INCREMENT NOT NULL, combattant_1_id INT NOT NULL, combattant_2_id INT NOT NULL, INDEX IDX_8D51E3982C77A260 (combattant_1_id), INDEX IDX_8D51E3983EC20D8E (combattant_2_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE combat ADD CONSTRAINT FK_8D51E3982C77A260 FOREIGN KEY (combattant_1_id) REFERENCES combattant (id)');
        $this->addSql('ALTER TABLE combat ADD CONSTRAINT FK_8D51E3983EC20D8E FOREIGN KEY (combattant_2_id) REFERENCES combattant (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE combat');
    }
}
